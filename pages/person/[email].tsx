import { useRouter } from "next/router";
import React, { useEffect, useRef, useState, useCallback } from "react";
import { Button, PageHeader, Descriptions, Input, message, Radio } from "antd";

import { withContextInitialized } from "../../components/hoc";
import CompanyCard from "../../components/molecules/CompanyCard";
import GenericList from "../../components/organisms/GenericList";
import OverlaySpinner from "../../components/molecules/OverlaySpinner";
import { usePersonInformation } from "../../components/hooks/usePersonInformation";

import { Company } from "../../constants/types";
import { ResponsiveListCard } from "../../constants";

const GENDER = {
  MALE: "male",
  FEMALE: "female",
};

interface IForm {
  name: string;
  phone: string;
  birthday: string;
  gender: string;
}

const PersonDetail = () => {
  const [isEditing, setIsEditing] = useState(false);
  const router = useRouter();
  const { load, loading, save, data } = usePersonInformation(
    router.query?.email as string,
    true
  );

  const [formValues, setFormValues] = useState<IForm>({
    name: data?.name,
    phone: data?.phone,
    birthday: data?.birthday,
    gender: data?.gender,
  });

  useEffect(() => {
    load();
  }, []);

  if (loading) {
    return (
      <OverlaySpinner title={`Loading ${router.query?.email} information`} />
    );
  }

  if (!data) {
    message.error("The user doesn't exist redirecting back...", 2, () =>
      router.push("/home")
    );
    return <></>;
  }

  const handleClickEdit = () => {
    setIsEditing(!isEditing);
  };

  const handleCancel = () => {
    setIsEditing(false);
    setFormValues({
      name: data?.name,
      phone: data?.phone,
      birthday: data?.birthday,
      gender: data?.gender,
    });
  };

  const handleChange = ({ target }) => {
    setFormValues({
      ...formValues,
      [target.name]: target.value,
    });
  };

  return (
    <>
      <PageHeader
        onBack={router.back}
        title="Person"
        subTitle="Profile"
        extra={[
          <Button
            style={{ padding: 0, margin: 0 }}
            type="link"
            href={data.website}
            target="_blank"
            rel="noopener noreferrer"
          >
            Visit website
          </Button>,
          isEditing && (
            <Button type="default" onClick={handleCancel}>
              cancel
            </Button>
          ),
          <Button type="default" onClick={handleClickEdit}>
            {isEditing ? "save" : "edit"}
          </Button>,
        ]}
      >
        {data && (
          <Descriptions size="small" column={1}>
            <Descriptions.Item label="Name">
              {isEditing ? (
                <Input
                  name="name"
                  value={formValues.name}
                  onChange={handleChange}
                  placeholder="Enter the name"
                />
              ) : (
                data.name
              )}
            </Descriptions.Item>
            <Descriptions.Item label="Gender">
              {isEditing ? (
                <Radio.Group
                  onChange={handleChange}
                  name="gender"
                  value={formValues.gender}
                >
                  <Radio value={GENDER.MALE}>{GENDER.MALE}</Radio>
                  <Radio value={GENDER.FEMALE}>{GENDER.FEMALE}</Radio>
                </Radio.Group>
              ) : (
                data.gender
              )}
            </Descriptions.Item>
            <Descriptions.Item label="Phone">
              {isEditing ? (
                <Input
                  name="phone"
                  type="tel"
                  value={formValues.phone}
                  onChange={handleChange}
                  placeholder="Enter the phone"
                />
              ) : (
                data.phone
              )}
            </Descriptions.Item>

            <Descriptions.Item label="Birthday">
              {isEditing ? (
                <Input
                  name="birthday"
                  type="date"
                  value={formValues.birthday}
                  onChange={handleChange}
                  placeholder="Enter the birthday"
                />
              ) : (
                data.birthday
              )}
            </Descriptions.Item>
          </Descriptions>
        )}
        <GenericList<Company>
          loading={loading}
          extra={ResponsiveListCard}
          data={data && data.companyHistory}
          ItemRenderer={({ item }: any) => <CompanyCard item={item} />}
          handleLoadMore={() => {}}
          hasMore={false}
        />
      </PageHeader>
    </>
  );
};

export default withContextInitialized(PersonDetail);
